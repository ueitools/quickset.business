﻿using Quickset.Model.CEC;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quickset.Business
{
    public class CalculateRuleMask
    {
        #region Properties
        private String m_ErrorMessage = String.Empty;
        public String ErrorMessage
        {
            get { return m_ErrorMessage; }
            set { m_ErrorMessage = value; }
        }
        #endregion
        private CECRuleMaskTypes GetRuleMask(String strKey, String strKeyCommand)
        {
            this.ErrorMessage = String.Empty;
            CECRuleMaskTypes m_RuleMaskType = CECRuleMaskTypes.DEFAULT;
            try
            {
                strKey = strKey.Trim().ToUpper();
                strKeyCommand = strKeyCommand.Trim().ToUpper();

                //Key with Command Combinatin is unique
                //based on this it sets weightage for each model
                //Curently we are considering only 14 CEC Key Commands Only
                if ((!String.IsNullOrEmpty(strKey)) && (!String.IsNullOrEmpty(strKeyCommand)))
                {
                    if ((strKey == "MUTE") && (strKeyCommand.Contains("0X44 0X43")))
                        m_RuleMaskType = CECRuleMaskTypes.MUTE;

                    //Volume                    
                    if ((strKey == "VOLUME UP") && (strKeyCommand.Contains("0X44 0X41")))
                        m_RuleMaskType = CECRuleMaskTypes.VOLUME;
                    if ((strKey == "VOLUME DOWN") && (strKeyCommand.Contains("0X44 0X42")))
                        m_RuleMaskType = CECRuleMaskTypes.VOLUME;

                    //Active Source
                    if ((strKey.Contains("<ACTIVE SOURCE>")) && (strKeyCommand.Contains("0X82")))
                        m_RuleMaskType = CECRuleMaskTypes.ACTIVE_SOURCE;

                    //Image View, Text View, Stand By
                    if ((strKey == "<STANDBY>") && (strKeyCommand.Contains("0X36")))
                        m_RuleMaskType = CECRuleMaskTypes.STANDBY;
                    if ((strKey == "<TEXT VIEW ON>") && (strKeyCommand.Contains("0X0D")))
                        m_RuleMaskType = CECRuleMaskTypes.TVO;
                    if ((strKey == "<IMAGE VIEW ON>") && (strKeyCommand.Contains("0X04")))
                        m_RuleMaskType = CECRuleMaskTypes.IVO;

                    //Power, Power On, Power Off
                    if ((strKey == "POWER") && (strKeyCommand.Contains("0X44 0X40")))
                        m_RuleMaskType = CECRuleMaskTypes.POWER;
                    if ((strKey == "POWER OFF FUNCTION") && (strKeyCommand.Contains("0X44 0X6C")))
                        m_RuleMaskType = CECRuleMaskTypes.POWER_OFF;
                    if ((strKey == "POWER ON FUNCTION") && (strKeyCommand.Contains("0X44 0X6D")))
                        m_RuleMaskType = CECRuleMaskTypes.POWER_ON;

                    //Audio Mode
                    if ((strKey == "<SET SYSTEM AUDIO MODE>") && (strKeyCommand.Contains("0X72 0X01")))
                        m_RuleMaskType = CECRuleMaskTypes.SYSTEM_AUDIO_MODE;
                    if ((strKey == "<SET STREAM PATH>") && (strKeyCommand.Contains("0X86")))
                        m_RuleMaskType = CECRuleMaskTypes.SET_STREAM_PATH;
                    if ((strKey == "<SYSTEM AUDIO MODE REQUEST>") && (strKeyCommand.Contains("0X70")))
                        m_RuleMaskType = CECRuleMaskTypes.SYSTEM_AUDIO_MODE_REQUEST;

                    if ((strKey == "<GIVE DEVICE POWER STATUS>") && (strKeyCommand.Contains("0X8F")))
                        m_RuleMaskType = CECRuleMaskTypes.GIVE_POWER_STATUS;
                }
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
            return m_RuleMaskType;
        }
        /// <summary>
        /// Gives Rule Mask Weightage based on CEC Key and CEC Command
        /// </summary>
        /// <param name="rulemaskType"></param>
        /// <param name="strKey">CEC Key</param>
        /// <param name="strKeyCommand">CEC Command</param>
        /// <returns></returns>
        public Int32 GetRuleMaskWeightage(ref CECRuleMaskTypes rulemaskType, String strKey, String strKeyCommand)
        {
            this.ErrorMessage = String.Empty;
            Int32 m_RuleMaskWeightage = 0;
            try
            {
                rulemaskType = GetRuleMask(strKey, strKeyCommand);
                m_RuleMaskWeightage = (Int32)rulemaskType;

            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
            return m_RuleMaskWeightage;
        }
        /// <summary>
        /// Gives CEC Rule Mask value for a given model
        /// Takes CEC Commands as input
        /// CEC commands Input must contains Columns "Status", "Key Name" and "Commands that worked"
        /// </summary>
        /// <param name="dtCECCommands"></param>
        /// <returns></returns>
        public Int32 GetRuleMask(DataTable dtCECCommands)
        {
            //Input: CECCommands Belongs to only one model at a time            
            Int32 intVolumeUpCount = 0, intVolumeDownCount = 0;
            Int32 rulemask = 0;
            Dictionary<String, Int32> m_KeyMaskWeighhtage = new Dictionary<String, Int32>();
            foreach (String key in Enum.GetNames(typeof(CECRuleMaskTypes)))
            {
                m_KeyMaskWeighhtage.Add(key, 0);
            }
            if (dtCECCommands != null)
            {
                foreach (DataRow cecfunction in dtCECCommands.Rows)
                {
                    String strKey = cecfunction["Key Name"].ToString().Trim().ToUpper();
                    String strKeyCommand = cecfunction["Commands that worked"].ToString().Trim().ToUpper();

                    if (cecfunction["Status"].ToString().Trim().ToUpper() == "PASS")
                    {
                        if ((strKey == "MUTE") && (strKeyCommand.Contains("0X44 0X43")))
                            m_KeyMaskWeighhtage["MUTE"] = (Int32)CECRuleMaskTypes.MUTE;

                        //Image View, Text View, Stand By
                        if ((strKey == "<STANDBY>") && (strKeyCommand.Contains("0X36")))
                            m_KeyMaskWeighhtage["STANDBY"] = (Int32)CECRuleMaskTypes.STANDBY;

                        if ((strKey == "<TEXT VIEW ON>") && (strKeyCommand.Contains("0X0D")))
                            m_KeyMaskWeighhtage["TVO"] = (Int32)CECRuleMaskTypes.TVO;
                        if ((strKey == "<IMAGE VIEW ON>") && (strKeyCommand.Contains("0X04")))
                            m_KeyMaskWeighhtage["IVO"] = (Int32)CECRuleMaskTypes.IVO;

                        //Power, Power On, Power Off
                        if ((strKey == "POWER") && (strKeyCommand.Contains("0X44 0X40")))
                            m_KeyMaskWeighhtage["POWER"] = (Int32)CECRuleMaskTypes.POWER;
                        if ((strKey == "POWER OFF FUNCTION") && (strKeyCommand.Contains("0X44 0X6C")))
                            m_KeyMaskWeighhtage["POWER_OFF"] = (Int32)CECRuleMaskTypes.POWER_OFF;
                        if ((strKey == "POWER ON FUNCTION") && (strKeyCommand.Contains("0X44 0X6D")))
                            m_KeyMaskWeighhtage["POWER_ON"] = (Int32)CECRuleMaskTypes.POWER_ON;

                        //Audio Mode
                        if ((strKey == "<SET SYSTEM AUDIO MODE>") && (strKeyCommand.Contains("0X72 0X01")))
                            m_KeyMaskWeighhtage["SYSTEM_AUDIO_MODE"] = (Int32)CECRuleMaskTypes.SYSTEM_AUDIO_MODE;
                        if ((strKey == "<SET STREAM PATH>") && (strKeyCommand.Contains("0X86")))
                            m_KeyMaskWeighhtage["SET_STREAM_PATH"] = (Int32)CECRuleMaskTypes.SET_STREAM_PATH;
                        if ((strKey == "<SYSTEM AUDIO MODE REQUEST>") && (strKeyCommand.Contains("0X70")))
                            m_KeyMaskWeighhtage["SYSTEM_AUDIO_MODE_REQUEST"] = (Int32)CECRuleMaskTypes.SYSTEM_AUDIO_MODE_REQUEST;

                        //Power Status
                        if ((strKey == "<GIVE DEVICE POWER STATUS>") && (strKeyCommand.Contains("0X8F")))
                            m_KeyMaskWeighhtage["GIVE_POWER_STATUS"] = (Int32)CECRuleMaskTypes.GIVE_POWER_STATUS;

                        if ((strKey == "VOLUME UP") && (strKeyCommand.Contains("0X44 0X41")))
                            intVolumeUpCount++;
                        if ((strKey == "VOLUME DOWN") && (strKeyCommand.Contains("0X44 0X42")))
                            intVolumeDownCount++;
                        if ((strKey.Contains("<ACTIVE SOURCE>")) && (strKeyCommand.Contains("0X82")))
                            m_KeyMaskWeighhtage["ACTIVE_SOURCE"] = (Int32)CECRuleMaskTypes.ACTIVE_SOURCE;                        
                    }
                }
                if ((intVolumeDownCount == 1) && (intVolumeUpCount == 1))
                {
                    m_KeyMaskWeighhtage["VOLUME"] = (Int32)CECRuleMaskTypes.VOLUME;
                }
                foreach (String item in m_KeyMaskWeighhtage.Keys)
                {
                    rulemask += m_KeyMaskWeighhtage[item];
                }
            }
            return rulemask;
        }
    }
}