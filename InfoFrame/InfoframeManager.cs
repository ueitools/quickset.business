﻿using Quickset.Model.InfoFrame;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ADOBusinessObject;
using System.Data;

namespace Quickset.Business.InfoFrame
{
    public  class InfoframeManager
    {
        public enum SaveStatus
        {
            Successfull = 1,
            FailedDueToDuplicate =2
        }
        public InfoframeManager()
        {
        }

        public static Dictionary<string, string> GetRegionNotations()
        {
            Dictionary<string, string> regionsWithCode = new Dictionary<string, string>();
            

            ADOBusinessObject.InfoFrameDataImport infoframeData = new ADOBusinessObject.InfoFrameDataImport();
            regionsWithCode = infoframeData.GetRegions();

            
            //modify the region name to show customised name
            foreach (string regionCode in regionsWithCode.Keys.ToList())
            {
                string regionName = regionsWithCode[regionCode];

                string formattedRegionName = String.Format("{0}({1})", regionName, regionCode);
                regionsWithCode[regionCode] = formattedRegionName;
            }
            return regionsWithCode;
        }

        public static Dictionary<string, string> GetCountries(string region)
        {
            Dictionary<string, string> countries = new Dictionary<string, string>();
            if (!String.IsNullOrEmpty(region))
            {
              ADOBusinessObject.InfoFrameDataImport infoframeData = new ADOBusinessObject.InfoFrameDataImport();
              countries = infoframeData.GetCountries(region);
              
             //modify the country name
                foreach (string countryCode in countries.Keys.ToList())
                {
                    string countryName = countries[countryCode];
                    string formattedCountryName = String.Format("{0}({1})", countryName, countryCode);

                    countries[countryCode] = formattedCountryName;
                }
            }
            return countries;
        }

        public static List<int> GetRuleNumbers()
        {
            InfoFrameDataImport infoFrameData = new InfoFrameDataImport();
            return infoFrameData.GetRuleNumbers();
        }
        /// <summary>
        /// Returns a dictionary object with InfoFrameType as key and corresponding field count as value.
        /// </summary>
        /// <returns></returns>
        public static Dictionary<string, int> GetInfoFrameTypesWithFieldCount()
        {
            InfoFrameDataImport infoFrameData = new InfoFrameDataImport();
            return infoFrameData.GetInfoFrameTypes();
        }

       
        public static List<string> GetManufacturers()
        {
            InfoFrameDataImport infoFrameData = new InfoFrameDataImport();
            return infoFrameData.GetManufacturers();
        }

        public static List<string> GetIDs(string searchText)
        {
            InfoFrameDataImport infoFrameData = new InfoFrameDataImport();
            return infoFrameData.GetIDs(searchText);
        }

        public static List<string> RemoveInvalidProviders(List<string> serviceProviders,out List<string> invalidProviders)
        {
            List<string> validProviders = new List<string>();
            invalidProviders = new List<string>();
            if (serviceProviders.Count > 0)
            {
                InfoFrameDataImport dataImport = new InfoFrameDataImport();
                invalidProviders =  dataImport.FindInvalidServiceProviders(serviceProviders);

                if (invalidProviders.Count > 0)
                {
                    validProviders = serviceProviders.Except(invalidProviders).ToList();
                }
                else
                {
                    validProviders = serviceProviders;
                }
               
            }

            return validProviders;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="deviceType">Not being used. Need to delete later after confirmation</param>
        /// <param name="SIFName"></param>
        /// <param name="SIFDescription"></param>
        /// <param name="vendorID"></param>
        /// <param name="ignoreInfoFrameRID"></param>
        /// <returns></returns>
        public bool CheckForDuplicateEntry(string deviceType,string SIFName, string SIFDescription, string vendorID,int ignoreInfoFrameRID)
        {
            InfoFrameDataImport infoFrameDataImport = new InfoFrameDataImport();

            //Check for duplicates
            List<InfoframeDBInput> infoFrameDataList = infoFrameDataImport.FindInfoframeData(SIFName, SIFDescription, vendorID);

            if (ignoreInfoFrameRID!=0)
            {
                InfoframeDBInput recordToIgnore = infoFrameDataList.Where(x => x.InfoFrameRecordDbID.Equals(ignoreInfoFrameRID)).FirstOrDefault();
               if(recordToIgnore!=null)
                    infoFrameDataList.Remove(recordToIgnore);
            }
            return (infoFrameDataList.Count > 0);
        }

        public SaveStatus SaveInfoframeData(InfoframeEntity entity,bool isUpdate)
        {
            string validationMessage;
            SaveStatus status = SaveStatus.Successfull;
            if (entity.Validate(out validationMessage))
            {
                InfoframeDBInput dbInput = entity.GenerateDBInput();
                InfoFrameDataImport infoFrameDataImport = new InfoFrameDataImport();
                if (isUpdate)
                {
                    dbInput.InfoFrameRecordDbID = entity.GetInfoFrameRecordID();
                   status =  (SaveStatus)infoFrameDataImport.UpdateInfoFrameData(dbInput.GetUpdateInputInTableFormat());
                }
                else
                {
                    
                   status = (SaveStatus) infoFrameDataImport.InsertInfoFrameData(dbInput.GetInputInTableFormat());
                }

            }

            return status;
        }

        
        public static DataTable GetInfoFrameReport()
        {
            InfoFrameDataImport infoFrameDataImport = new InfoFrameDataImport();
          List<InfoframeDBInput> infoFrameRecords = infoFrameDataImport.GetInfoFrameRecords();

          if (infoFrameRecords.Count > 0)
              return GenerateDataTable(infoFrameRecords);
          else
              return null;
               
        }

        public static DataTable GetInfoFrameReport(string sifName,string sifDesc,string manufacturer)
        {
            InfoFrameDataImport infoFrameDataImport = new InfoFrameDataImport();
            List<InfoframeDBInput> infoFrameRecords = infoFrameDataImport.FindInfoframeData(sifName,sifDesc,manufacturer);

            if (infoFrameRecords.Count > 0)
                return GenerateDataTable(infoFrameRecords);
            else
                return null;
        }
        


        public static DataTable GenerateDataTable(List<InfoframeDBInput> infoFrameRecords)
        {
            DataTable dtReport = new DataTable();
            dtReport.Columns.Add("RID");
            dtReport.Columns.Add("EntryDataSize");
            dtReport.Columns.Add("UEIDeviceTypeID");
            dtReport.Columns.Add("InfoFrameRawData");
            dtReport.Columns.Add("UEIInfoFrameType");
            dtReport.Columns.Add("InfoFrameFieldCount");
            dtReport.Columns.Add("SIFName");
            dtReport.Columns.Add("SIFDescription");
            dtReport.Columns.Add("Manufacturer&VendorID(s)");
            dtReport.Columns.Add("ProvidersCount");
            dtReport.Columns.Add("Providers");

            dtReport.Columns.Add("ControlBlockCount");
            dtReport.Columns.Add("Region+ControlBlock");

            foreach (InfoframeDBInput dbInput in infoFrameRecords)
            {
                DataRow newRecord = dtReport.NewRow();
                newRecord["RID"] = dbInput.InfoFrameRecordDbID;
                newRecord["EntryDataSize"] = dbInput.EntryDataSize;
                newRecord["UEIDeviceTypeID"] = dbInput.UEIDeviceType;
                newRecord["InfoFrameRawData"] = dbInput.InfoFrameRawData;
                newRecord["UEIInfoFrameType"] = dbInput.InfoFrameType;
                newRecord["InfoFrameFieldCount"] = dbInput.InfoFrameFieldCount;
                newRecord["SIFName"] = dbInput.SIFName;
                newRecord["SIFDescription"] = dbInput.SIFDescription;
                newRecord["Manufacturer&VendorID(s)"] = dbInput.VendorID;
                newRecord["ProvidersCount"] = dbInput.ProviderCount;
                newRecord["Providers"] = dbInput.Providers;
                newRecord["ControlBlockCount"] = dbInput.ControlBlockCount;
                newRecord["Region+ControlBlock"] = dbInput.ControlBlock;

                dtReport.Rows.Add(newRecord);
            }
            dtReport.AcceptChanges();

            return dtReport;
        }

        //public bool BulkImportInfoFrameData(List<InfoframeEntity> infoFrameRecords)
        //{
        //    if (infoFrameRecords.Count > 0)
        //    {
        //        List<InfoframeDBInput> infoFrameDBInput = new List<InfoframeDBInput>();
        //        foreach (InfoframeEntity entity in infoFrameRecords)
        //        {
        //            infoFrameDBInput.Add(entity.GenerateDBInput());
        //        }

        //       DataTable dtInfoFrameRecords =  GenerateDataTable(infoFrameDBInput);

        //       if (dtInfoFrameRecords.Rows.Count > 0)
        //       {
        //           InfoFrameDataImport dataImport = new InfoFrameDataImport();
        //           dataImport.BulkImportOfInfoFrameData(dtInfoFrameRecords);

        //           return true;
        //       }
        //    }

        //    return false;
        //}
    }
}
