﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
public class Logging
{
    private static String logFile = String.Empty;
    private static String errorlogFile = String.Empty;
    public static String reportLogFile = String.Empty;
    private const String _UEITOOLSROOT = "HKEY_CURRENT_USER\\Software\\UEITools\\";
    private const String _WORKINGDIR = "WorkingDirectory";
    private const String _DEFAULT_WORKINGDIR = "C:\\Working";

    static Logging()
    {
        logFile = GetSettings("Log");
        errorlogFile = GetSettings("ErrorLog");
        reportLogFile = GetSettings("ReportLog");
        //Check if the Log file Directory is exist
        //if not create
        IsDirectoryPresent(StripDirectoryName(logFile), true);

        //Check if the Error Log file Directory is exist
        //if not create
        IsDirectoryPresent(StripDirectoryName(errorlogFile), true);
    }
    public static void ResetSettings()
    {
        logFile = GetSettings("Log");
        errorlogFile = GetSettings("ErrorLog");
        reportLogFile = GetSettings("ReportLog");
        //Check if the Log file Directory is exist
        //if not create
        IsDirectoryPresent(StripDirectoryName(logFile), true);

        //Check if the Error Log file Directory is exist
        //if not create
        IsDirectoryPresent(StripDirectoryName(errorlogFile), true);
    }
    private static String GetfilefromPath(String path)
    {
        try
        {
            String formattedDate = String.Empty;
            String file = String.Empty;
            Int32 indexOfPeriod;
            formattedDate = "(" + DateTime.Now.ToString("dd - MM - yyyy") + ")";
            indexOfPeriod = path.LastIndexOf(".");
            file = path.Insert(indexOfPeriod, formattedDate);
            return file;
        }
        catch (Exception ex)
        {
            //Write To Error Log
            WriteToErrorLogFile(ex);
            return "";
        }
        finally
        {
        }
    }
    private static String GetSettings(String val)
    {
        try
        {
            String path = String.Empty;
            switch (val)
            {
                case "Log":
                    path = GetfilefromPath((string)Microsoft.Win32.Registry.GetValue(
                           _UEITOOLSROOT, _WORKINGDIR, _DEFAULT_WORKINGDIR) + "\\Log.txt");
                    break;
                case "ErrorLog":
                    path = GetfilefromPath((string)Microsoft.Win32.Registry.GetValue(
                           _UEITOOLSROOT, _WORKINGDIR, _DEFAULT_WORKINGDIR) + "\\ErrorLog.txt");
                    break;
                case "ReportLog":
                    path = GetfilefromPath((string)Microsoft.Win32.Registry.GetValue(
                           _UEITOOLSROOT, _WORKINGDIR, _DEFAULT_WORKINGDIR) + "\\ReportLog.txt");
                    break;
            }
            return path;
        }
        catch (Exception ex)
        {
            //Write To Error Log
            WriteToErrorLogFile(ex);
            return "";
        }
        finally
        {
        }
    }

    #region Write To File
    public static void WriteToLogFile(String message, Boolean tm)
    {
        String _logFileName = String.Empty;
        if (IsDirectoryPresent(StripDirectoryName(logFile), true))
        {
            FileStream fs = null;
            StreamWriter sw = null;
            try
            {
                fs = new FileStream(logFile, FileMode.Append, FileAccess.Write);
                sw = new StreamWriter(fs);
                if (tm == true)
                {
                    //sw.WriteLine("===================================================");
                    sw.WriteLine(message + "\t" + DateTime.Now.ToString());
                    //sw.WriteLine("===================================================");
                }
                else
                {
                    // sw.WriteLine("===================================================");
                    sw.WriteLine(message);
                    //sw.WriteLine("===================================================");
                }
                //sw.WriteLine("");
            }
            catch (Exception ex)
            {
                WriteToErrorLogFile(ex);
            }
            finally
            {
                if (sw != null)
                {
                    sw.Close();
                }
                if (fs != null)
                {
                    fs.Close();
                }
            }
        }
    }
    public static void WriteToErrorLogFile(Exception sourceException)
    {
        if (IsDirectoryPresent(StripDirectoryName(errorlogFile), true))
        {
            FileStream fs = null;
            StreamWriter sw = null;
            try
            {
                fs = new FileStream(errorlogFile, FileMode.Append, FileAccess.Write);
                sw = new StreamWriter(fs);
                sw.WriteLine("==================================================================");
                sw.WriteLine("ERROR OCCOURED IN:" + sourceException.Source);
                sw.WriteLine("ERROR DESCRPTION:" + sourceException.Message);
                sw.WriteLine("ERROR DESCRPTION:" + sourceException.StackTrace);
                sw.WriteLine("ERROR DATE TIME:" + System.DateTime.Now.ToString());
                sw.WriteLine("==================================================================");
                sw.WriteLine("");
            }
            finally
            {
                if (sw != null)
                {
                    sw.Close();
                }

                if (fs != null)
                {
                    fs.Close();
                }
            }
        }
    }
    public static Boolean IsDirectoryPresent(String directory, Boolean create)
    {
        try
        {
            if (!Directory.Exists(directory))
            {
                if (create == true)
                {
                    Directory.CreateDirectory(directory);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
        }
        catch (Exception ex)
        {
            //Write to Error Log
            WriteToErrorLogFile(ex);
            return false;
        }
        finally
        {
        }
    }
    public static String StripDirectoryName(String path)
    {
        string direcoryPath = @"";
        int indexOfLastSlash = 0;

        try
        {
            indexOfLastSlash = path.LastIndexOf(@"\");
            direcoryPath = path.Substring(0, indexOfLastSlash);
            return direcoryPath;
        }
        catch (Exception ex)
        {
            //Write to Error Log file
            WriteToErrorLogFile(ex);
            return "";
        }
        finally
        {
        }
    }
    #endregion

    #region Set Working Directory
    public static void SetWorkingDirectory()
    {
        SetWorkingDirectory("");
        ResetSettings();
    }
    public static void SetWorkingDirectory(String subDir)
    {
        FolderBrowserDialog dlg = new FolderBrowserDialog();
        dlg.Description = "Choose your working directory";
        dlg.SelectedPath = GetWorkingDirectory(subDir);

        if (dlg.ShowDialog() == DialogResult.OK)
        {
            if (dlg.SelectedPath.EndsWith("\\") == false)
            {
                dlg.SelectedPath += "\\";
            }

            Registry.SetValue(_UEITOOLSROOT + subDir, _WORKINGDIR, dlg.SelectedPath, RegistryValueKind.String);
        }
    }
    public static String GetWorkingDirectory()
    {
        return GetWorkingDirectory("");
    }
    public static String GetWorkingDirectory(String subDir)
    {
        return (string)Registry.GetValue(_UEITOOLSROOT + subDir, _WORKINGDIR, _DEFAULT_WORKINGDIR);
    }
    #endregion
}