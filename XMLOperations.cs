﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;

namespace Quickset.Business
{
    public class XMLOperations : IDisposable
    {
        #region Variables
        private XmlDocument document = null;
        private XmlElement root = null;
        private XmlElement XMLParentNode = null;
        private XmlAttribute XMLParentAttribute = null;
        private XmlElement XMLChildNode = null;
        private XmlAttribute XMLChildAttribute = null;
        private XmlElement XMLFirstChild = null;
        private XmlAttribute XMlFirstChildAttribute = null;
        private XmlElement XMLSecondChild = null;
        private XmlAttribute XMlSecondChildAttribute = null;
        private XmlElement XMLThirdChild = null;
        private XmlAttribute XMlThirdChildAttribute = null;
        #endregion

        #region Properties
        private String strParentNodeName = String.Empty;
        public String ParentNode
        {
            get
            {
                return strParentNodeName;
            }
            set
            {
                strParentNodeName = value;
                BuildParentNode();
            }
        }
        private String strParentAttributeName = String.Empty;
        public String ParentAttributeName
        {
            get
            {
                return strParentAttributeName;
            }
            set
            {
                strParentAttributeName = value;
                AddParentAttributeName();
            }
        }
        private String strParentAttributeValue = String.Empty;
        public String ParentAttributeValue
        {
            get
            {
                return strParentAttributeValue;
            }
            set
            {
                strParentAttributeValue = value;
                AddParentAttributeValue();
            }
        }
        private String strChildNodeName = String.Empty;
        public String ChildNode
        {
            get
            {
                return strChildNodeName;
            }
            set
            {
                strChildNodeName = value;
                BuildChildNode();
            }
        }
        private String strChildAttributeName = String.Empty;
        public String ChildAttributeName
        {
            get
            {
                return strChildAttributeName;
            }
            set
            {
                strChildAttributeName = value;
                AddChildAttributeName();
            }
        }
        private String strChildAttributeValue = String.Empty;
        public String ChildAttributeValue
        {
            get
            {
                return strChildAttributeValue;
            }
            set
            {
                strChildAttributeValue = value;
                AddChildAttributeValue();
            }
        }
        private String strParentValue = String.Empty;
        public String ParentValue
        {
            get
            {
                return strParentValue;
            }
            set
            {
                strParentValue = value;
                XmlCDataSection CDATA = document.CreateCDataSection(strParentValue);
                XMLParentNode.AppendChild(CDATA);
            }
        }
        private String strChildValue = String.Empty;
        public String ChildValue
        {
            get
            {
                return strChildValue;
            }
            set
            {
                strChildValue = value;
                XmlCDataSection CDATA = document.CreateCDataSection(strChildValue);
                XMLChildNode.AppendChild(CDATA);
            }
        }
        private EncodingType m_XMLEncoding = EncodingType.UTF8;
        public EncodingType XMLEncoding
        {
            get { return m_XMLEncoding; }
            set { m_XMLEncoding = value; }
        }
        private String m_XMLVersion = "1.0";
        public String XMLVersion
        {
            get { return m_XMLVersion; }
            set { m_XMLVersion = value; }
        }
        private String m_StandAlone = null;
        public String StandAlone
        {
            get { return m_StandAlone; }
            set { m_StandAlone = value; }
        }
        #endregion

        #region XMLOperations
        public XMLOperations()
        {
            this.strParentNodeName = String.Empty;
            this.strParentValue = String.Empty;
            this.strParentAttributeName = String.Empty;
            this.strParentAttributeValue = String.Empty;
            this.strChildNodeName = String.Empty;
            this.strChildValue = String.Empty;
            this.strChildAttributeName = String.Empty;
            this.strChildAttributeValue = String.Empty;
            BuildRoot();
        }
        public XMLOperations(String strRootNode)
        {
            this.strParentNodeName = String.Empty;
            this.strParentValue = String.Empty;
            this.strParentAttributeName = String.Empty;
            this.strParentAttributeValue = String.Empty;
            this.strChildNodeName = String.Empty;
            this.strChildValue = String.Empty;
            this.strChildAttributeName = String.Empty;
            this.strChildAttributeValue = String.Empty;
            BuildDefinedRoot(strRootNode);
        }
        public XMLOperations(String strRootNode, EncodingType encodingType)
        {
            this.XMLEncoding = encodingType;
            this.strParentNodeName = String.Empty;
            this.strParentValue = String.Empty;
            this.strParentAttributeName = String.Empty;
            this.strParentAttributeValue = String.Empty;
            this.strChildNodeName = String.Empty;
            this.strChildValue = String.Empty;
            this.strChildAttributeName = String.Empty;
            this.strChildAttributeValue = String.Empty;
            BuildDefinedRoot(strRootNode);
        }
        #endregion

        #region CreateXML
        private void BuildRoot()
        {
            document = new XmlDocument();
            //XmlDeclaration XMLDeclaration = document.CreateXmlDeclaration(XMLVersion, null, StandAlone);            
            XmlDeclaration XMLDeclaration = document.CreateXmlDeclaration(XMLVersion, null, null);
            document.AppendChild(XMLDeclaration);
            root = (XmlElement)document.CreateElement("Root");
            root.SetAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            root.SetAttribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
            document.AppendChild(root);
        }
        private void BuildDefinedRoot(String strRootNode)
        {
            document = new XmlDocument();
            //XmlDeclaration XMLDeclaration = document.CreateXmlDeclaration(XMLVersion, GetEncodingType(), StandAlone);
            XmlDeclaration XMLDeclaration = document.CreateXmlDeclaration(XMLVersion, null, null);
            document.AppendChild(XMLDeclaration);
            root = (XmlElement)document.CreateElement(strRootNode);
            root.SetAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            root.SetAttribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
            document.AppendChild(root);
        }
        private String GetEncodingType()
        {
            String strEncoding = null;
            switch (XMLEncoding)
            {
                case EncodingType.UTF7:
                    strEncoding = Encoding.UTF7.HeaderName;
                    break;
                case EncodingType.UTF8:
                    strEncoding = Encoding.UTF8.HeaderName;
                    break;
                case EncodingType.Unicode:
                    strEncoding = Encoding.Unicode.HeaderName;
                    break;
                case EncodingType.UTF16:
                    strEncoding = Encoding.GetEncoding(1200).HeaderName;
                    break;
                case EncodingType.UTF32:
                    strEncoding = Encoding.UTF32.HeaderName;
                    break;
                default:
                    strEncoding = Encoding.Default.HeaderName;
                    break;
            }
            return strEncoding;
        }
        private void BuildParentNode()
        {
            XMLParentNode = (XmlElement)document.CreateElement(ParentNode);
        }
        private void AddParentAttributeName()
        {
            XMLParentAttribute = XMLParentNode.SetAttributeNode(ParentAttributeName, String.Empty);
        }
        private void AddParentAttributeValue()
        {
            XMLParentAttribute.Value = ParentAttributeValue;
        }
        public void AppendParent()
        {
            root.AppendChild(XMLParentNode);
            XMLParentAttribute = null;
            XMLParentNode = null;
        }
        private void BuildChildNode()
        {
            if (this.XMLSecondChild != null)
            {
                this.XMLThirdChild = this.document.CreateElement(this.ChildNode);
            }
            else if (this.XMLFirstChild != null)
            {
                this.XMLSecondChild = this.document.CreateElement(this.ChildNode);
            }
            else if (this.XMLChildNode != null)
            {
                this.XMLFirstChild = this.document.CreateElement(this.ChildNode);
            }
            else
            {
                this.XMLChildNode = this.document.CreateElement(this.ChildNode);
            }
        }
        private void AddChildAttributeName()
        {
            if (this.XMLThirdChild != null)
            {
                this.XMlThirdChildAttribute = this.XMLThirdChild.SetAttributeNode(this.ChildAttributeName, String.Empty);
            }
            else if (this.XMLSecondChild != null)
            {
                this.XMlSecondChildAttribute = this.XMLSecondChild.SetAttributeNode(this.ChildAttributeName, String.Empty);
            }
            else if (this.XMLFirstChild != null)
            {
                this.XMlFirstChildAttribute = this.XMLFirstChild.SetAttributeNode(this.ChildAttributeName, String.Empty);
            }
            else
            {
                this.XMLChildAttribute = this.XMLChildNode.SetAttributeNode(this.ChildAttributeName, String.Empty);
            }
        }
        private void AddChildAttributeValue()
        {
            if (this.XMLThirdChild != null)
            {
                this.XMlThirdChildAttribute.Value = this.ChildAttributeValue;
            }
            else if (this.XMLSecondChild != null)
            {
                this.XMlSecondChildAttribute.Value = this.ChildAttributeValue;
            }
            else if (this.XMLFirstChild != null)
            {
                this.XMlFirstChildAttribute.Value = this.ChildAttributeValue;
            }
            else
            {
                this.XMLChildAttribute.Value = this.ChildAttributeValue;
            }
        }
        public void AppendChild()
        {
            if (this.XMLParentNode != null)
            {
                if (this.XMLThirdChild != null)
                {
                    this.XMLSecondChild.AppendChild(this.XMLThirdChild);
                    this.XMlThirdChildAttribute = null;
                    this.XMLThirdChild = null;
                }
                else if (this.XMLSecondChild != null)
                {
                    this.XMLFirstChild.AppendChild(this.XMLSecondChild);
                    this.XMlSecondChildAttribute = null;
                    this.XMLSecondChild = null;
                }
                else if (this.XMLFirstChild != null)
                {
                    this.XMLChildNode.AppendChild(this.XMLFirstChild);
                    this.XMlFirstChildAttribute = null;
                    this.XMLFirstChild = null;
                }
                else
                {
                    this.XMLParentNode.AppendChild(this.XMLChildNode);
                    this.XMLChildAttribute = null;
                    this.XMLChildNode = null;
                }
            }
        }
        public bool HasElement(string name)
        {
            if (!String.IsNullOrEmpty(name))
            {
                if (this.root.GetElementsByTagName(name).Count > 0)
                    return true;
            }

            return false;
        }
        public String XMLGenerator()
        {
            XmlWriterSettings m_Settings = new XmlWriterSettings();
            switch (XMLEncoding)
            {
                case EncodingType.UTF7:
                    m_Settings.Encoding = Encoding.UTF7;
                    break;
                case EncodingType.UTF8:
                    m_Settings.Encoding = Encoding.UTF8;
                    break;
                case EncodingType.Unicode:
                    m_Settings.Encoding = Encoding.Unicode;
                    break;
                case EncodingType.UTF16:
                    m_Settings.Encoding = Encoding.GetEncoding(1200);
                    break;
                case EncodingType.UTF32:
                    m_Settings.Encoding = Encoding.UTF32;
                    break;
                default:
                    m_Settings.Encoding = Encoding.Default;
                    break;
            }
            StringBuilder strResult = new StringBuilder();
            XmlWriter xmlWriter = XmlWriter.Create(strResult, m_Settings);
            document.WriteTo(xmlWriter);
            xmlWriter.Close();
            return (strResult.ToString());
            //return (document.InnerXml);
        }

        public void Dispose()
        {
            root = null;
            document = null;
        }
        #endregion

        #region Encoding Types
        public enum EncodingType
        {
            UTF7,
            UTF8,
            UTF16,
            UTF32,
            Unicode
        }
        #endregion
    }
}