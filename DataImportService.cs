﻿using ADOBusinessObject;
using Quickset.Model.CEC;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quickset.Business
{
    public class DataImportService
    {
        #region Variables
        private UEI2Interface m_ADOService = null;
        private XMLOperations m_XML = null;
        #endregion

        #region Properties
        private String m_ErrorMessage = String.Empty;
        public String ErrorMessage
        {
            get { return m_ErrorMessage; }
            set { m_ErrorMessage = value; }
        }
        #endregion

        #region Constructor
        public DataImportService()
        {
            this.m_ADOService = new UEI2Interface();
        }
        #endregion        

        #region IP Data Import Layer - 1
        DataTable FillIPCapturedData(DataTable datatableFrom, DataTable datatableTo)
        {
            this.ErrorMessage = String.Empty;
            try
            {                
                DataRow dr = null;
                for (Int32 intRows = 0; intRows < datatableFrom.Rows.Count; intRows++)
                {
                    dr = datatableTo.NewRow();
                    for (Int32 intColumn = 0; intColumn < datatableFrom.Columns.Count; intColumn++)
                    {
                        if (dr.Table.Columns.Contains(datatableFrom.Columns[intColumn].ColumnName))
                        {
                            try
                            {
                                dr[datatableFrom.Columns[intColumn].ColumnName] = datatableFrom.Rows[intRows][intColumn];
                            }
                            catch
                            {
                            }
                        }
                    }
                    datatableTo.Rows.Add(dr);
                    dr = null;
                }                
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
            return datatableTo;
        }
        public DataSet IPDataImport_Layer1(DataTable captureDevices, ref String strStatusMessage)
        {
            DataSet dsResult = null;
            this.ErrorMessage = String.Empty;
            try
            {
                DataTable datatableCaptureDevices = null;

                //Get SchemaTable from Server
                strStatusMessage = "Reading Schema Table for " + captureDevices.TableName;
                //Need to provide Table type Name
                datatableCaptureDevices = this.m_ADOService.GetCapturedTableType("IPInputData");
                
                //Update Columns
                if (datatableCaptureDevices != null && captureDevices != null)
                {
                    for (int i = 0; i < datatableCaptureDevices.Columns.Count-1; i++)
                    {
                        captureDevices.Columns[i].ColumnName = datatableCaptureDevices.Columns[i].ColumnName;
                    }
                }

                //Fill DataTable
                strStatusMessage = "Filling " + captureDevices.TableName + " from IP Data Import Template";
                datatableCaptureDevices = FillIPCapturedData(captureDevices, datatableCaptureDevices);
                
                
                strStatusMessage = "Importing IP Captured Details...";
                dsResult = this.m_ADOService.ImportIPData_Layer1(datatableCaptureDevices);
                this.ErrorMessage = this.m_ADOService.ErrorMessage;

                if (dsResult != null)
                {                    
                    dsResult.Tables[0].TableName = "IP Capture Details";
                }
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
            return dsResult;
        }
        #endregion

        #region CEC EDID Data Import - Layer - 1
        private DataTable FillCECCapturedData(DataTable datatableFrom, DataTable datatableTo)
        {
            DataRow dr = null;
            for (Int32 intRows = 0; intRows < datatableFrom.Rows.Count; intRows++)
            {
                dr = datatableTo.NewRow();
                for (Int32 intColumn = 0; intColumn < datatableFrom.Columns.Count; intColumn++)
                {                    
                    if (dr.Table.Columns.Contains(datatableFrom.Columns[intColumn].ColumnName))
                        try
                        {
                            //Validation for Sub Devices
                            if (datatableFrom.Columns[intColumn].ColumnName.ToUpper().Contains("SUBDEVICE"))
                            {
                                //Check if Sub Device is TV make it Television
                                if (datatableFrom.Rows[intRows][intColumn].ToString().Trim().ToUpper() == "TV")
                                    dr[datatableFrom.Columns[intColumn].ColumnName] = "Television";
                                //Check if Sub Device is PROJECTION make it Rear Projection
                                else if (datatableFrom.Rows[intRows][intColumn].ToString().Trim().ToUpper() == "PROJECTION")
                                    dr[datatableFrom.Columns[intColumn].ColumnName] = "Rear Projection";
                                //Check if Sub Device is AV Receiver make it A/V Receiver
                                else if (datatableFrom.Rows[intRows][intColumn].ToString().Trim().ToUpper() == "AV RECEIVER")
                                    dr[datatableFrom.Columns[intColumn].ColumnName] = "A/V Receiver"; 
                                else
                                    dr[datatableFrom.Columns[intColumn].ColumnName] = datatableFrom.Rows[intRows][intColumn];
                            }
                            else if (datatableFrom.Columns[intColumn].ColumnName.ToUpper().Contains("RULE NO"))
                            {
                                if (!String.IsNullOrEmpty(datatableFrom.Rows[intRows][intColumn].ToString()))
                                {
                                    dr[datatableFrom.Columns[intColumn].ColumnName] = Int32.Parse(datatableFrom.Rows[intRows][intColumn].ToString());
                                }
                                else
                                {
                                    //if Rule No is not Supplied it will set to "Dummy Rule:999"
                                    dr[datatableFrom.Columns[intColumn].ColumnName] = 999;
                                }
                            }
                            else
                                dr[datatableFrom.Columns[intColumn].ColumnName] = datatableFrom.Rows[intRows][intColumn];
                            //Rule No
                        }
                        catch 
                        {
                            if (datatableFrom.Columns[intColumn].ColumnName.Contains("Date & Time"))
                            {
                                dr[datatableFrom.Columns[intColumn].ColumnName] = DBNull.Value;
                            }
                        }
                }
                datatableTo.Rows.Add(dr);
                dr = null;
            }
            return datatableTo;
        }
        public DataSet CECDataImport(DataTable captureDevices, DataTable captureKeys, DataTable captureFunctions, ref String strStatusMessage)
        {
            DataSet dsResult = null;
            this.ErrorMessage = String.Empty;           
            try
            {
                DataTable datatableCaptureDevices = null, datatableCaptureKeys = null, datatableCaptureFunctions = null;
                //Get SchemaTable from Server
                strStatusMessage            = "Reading Schema Table for " + captureDevices.TableName;
                datatableCaptureDevices     = this.m_ADOService.GetCapturedTableType("CaptureDeviceDetails");
                strStatusMessage            = "Reading Schema Table for " + captureDevices.TableName;
                datatableCaptureKeys        = this.m_ADOService.GetCapturedTableType("CECKeyDetails");
                strStatusMessage            = "Reading Schema Table for " + captureDevices.TableName;
                datatableCaptureFunctions   = this.m_ADOService.GetCapturedTableType("CECCommandDetails");

                //Fill DataTable
                strStatusMessage            = "Filling "+ captureDevices.TableName +" from CEC Template";
                datatableCaptureDevices     = FillCECCapturedData(captureDevices, datatableCaptureDevices);
                strStatusMessage            = "Filling " + captureKeys.TableName + " from CEC Template";
                datatableCaptureKeys        = FillCECCapturedData(captureKeys, datatableCaptureKeys);
                strStatusMessage            = "Filling " + captureFunctions.TableName + " from CEC Template";
                datatableCaptureFunctions   = FillCECCapturedData(captureFunctions, datatableCaptureFunctions);

                strStatusMessage = "Importing CEC Captured Details...";
                dsResult = this.m_ADOService.ImportCECData(datatableCaptureDevices, datatableCaptureKeys, datatableCaptureFunctions);
                this.ErrorMessage = this.m_ADOService.ErrorMessage;

                if (dsResult != null)
                {
                    dsResult.Tables[0].TableName = "Device-Capture Details";
                    dsResult.Tables[2].TableName = "CEC Keys";
                    dsResult.Tables[1].TableName = "Command List";
                }                
            }
            catch (Exception ex)
            {                
                this.ErrorMessage = ex.Message;
            }
            return dsResult;
        }
        #endregion

        #region Get Report Layer - 1 and Layer - 2
        public DataSet GetCECReport_Layer1(DataTable devicetypes, DataTable locationTypes, String brands, String models)
        {
            DataSet dsReport = null;
            DataTable dtCEC = null;
            DataTable dtCECKeys = null;
            DataTable dtCECCommandList = null;
            DataTable dtLocationWithCode = null;
            this.ErrorMessage = String.Empty;
            try
            {
                //Get All CEC Tested Devices
                dtCEC = this.m_ADOService.GetMasterReport(devicetypes, locationTypes, brands, models);
                if (dtCEC != null)
                    dtCEC.TableName = "CEC";
                //Get All CEC Keys
                dtCECKeys = this.m_ADOService.GetCECKeys(devicetypes, locationTypes, brands, models);
                if (dtCECKeys != null)
                    dtCECKeys.TableName = "CEC Keys";
                //Get All CEC Command List for Tested Devices
                dtCECCommandList = this.m_ADOService.GetCECCommandList(devicetypes, locationTypes, brands, models);
                if (dtCECCommandList != null)
                    dtCECCommandList.TableName = "CEC Command List";
                //Get All Locations with Code
                dtLocationWithCode = this.m_ADOService.GetLocationWithCodeFromServer();
                if (dtLocationWithCode != null)
                    dtLocationWithCode.TableName = "Locations";

                dsReport = new DataSet();
                if(dtCEC != null)
                dsReport.Tables.Add(dtCEC);
                if(dtCECKeys != null)
                dsReport.Tables.Add(dtCECKeys);
                if(dtCECCommandList != null)
                dsReport.Tables.Add(dtCECCommandList);
                dsReport.Tables.Add(dtLocationWithCode);

                this.ErrorMessage = this.m_ADOService.ErrorMessage;
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
            return dsReport;
        }
        public DataSet GetEDIDReport_Layer1(DataTable devicetypes, DataTable locationTypes, String brands, String models)
        {
            DataSet dsReport = null;
            DataTable dtCEC = null;
            DataTable dtLocationWithCode = null;
            this.ErrorMessage = String.Empty;
            try
            {
                //Get All CEC Tested Devices
                dtCEC = this.m_ADOService.GetMasterReport(devicetypes, locationTypes, brands, models);
                if (dtCEC != null)
                    dtCEC.TableName = "CEC";
                //Get All Locations with Code
                dtLocationWithCode = this.m_ADOService.GetLocationWithCodeFromServer();
                if (dtLocationWithCode != null)
                    dtLocationWithCode.TableName = "Locations";

                dsReport = new DataSet();
                if(dtCEC != null)
                dsReport.Tables.Add(dtCEC);
                dsReport.Tables.Add(dtLocationWithCode);
                this.ErrorMessage = this.m_ADOService.ErrorMessage;
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
            return dsReport;
        }
        public DataSet GetIPReport_Layer2(DataTable devicetypes, DataTable locationTypes, String brands, String models)
        {
            DataSet dsReport = null;
            DataTable dtIP = null;
            DataTable dtLocationWithCode = null;
            this.ErrorMessage = String.Empty;
            try
            {
                //Get All Locations with Code
                dtLocationWithCode = this.m_ADOService.GetLocationWithCodeFromServer();
                if (dtLocationWithCode != null)
                    dtLocationWithCode.TableName = "Locations";

                //Get All IP Data
                dtIP = this.m_ADOService.GetIPData_Layer2(); //this.m_ADOService.GetIPFromServer(String.Empty, String.Empty);
                if (dtIP != null)
                    dtIP.TableName = "IP";
                dsReport = new DataSet();
                if(dtIP != null)
                dsReport.Tables.Add(dtIP);
                dsReport.Tables.Add(dtLocationWithCode);

                this.ErrorMessage = this.m_ADOService.ErrorMessage;
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
            return dsReport;
        }
        public DataSet GetIPReport_Layer1(DataTable devicetypes, DataTable locationTypes, String brands, String models)
        {
            DataSet dsReport = null;
            DataTable dtIP = null;
            DataTable dtLocationWithCode = null;
            this.ErrorMessage = String.Empty;
            try
            {
                //Get All Locations with Code
                dtLocationWithCode = this.m_ADOService.GetLocationWithCodeFromServer();
                if (dtLocationWithCode != null)
                    dtLocationWithCode.TableName = "Locations";

                //Get All IP Data
                dtIP = this.m_ADOService.GetIPData_Layer1();
                if (dtIP != null)
                    dtIP.TableName = "IP";
                dsReport = new DataSet();
                if(dtIP != null)
                dsReport.Tables.Add(dtIP);
                dsReport.Tables.Add(dtLocationWithCode);

                this.ErrorMessage = this.m_ADOService.ErrorMessage;
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
            return dsReport;
        }
        public DataSet GetCECReport_Layer2(DataTable devicetypes, DataTable locationTypes, String brands, String models)
        {
            DataSet dsReport = null;
            DataTable dtCEC = null;
            DataTable dtLocationWithCode = null;
            this.ErrorMessage = String.Empty;
            try
            {
                //Get All Locations with Code
                dtLocationWithCode = this.m_ADOService.GetLocationWithCodeFromServer();
                if (dtLocationWithCode != null)
                    dtLocationWithCode.TableName = "Locations";

                //Get All IP Data
                dtCEC = this.m_ADOService.GetCECData_Layer2(); //this.m_ADOService.GetIPFromServer(String.Empty, String.Empty);
                if (dtCEC != null)
                    dtCEC.TableName = "CEC";

                dsReport = new DataSet();
                if (dtCEC != null)
                    dsReport.Tables.Add(dtCEC);
                dsReport.Tables.Add(dtLocationWithCode);

                this.ErrorMessage = this.m_ADOService.ErrorMessage;
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
            return dsReport;
        }
        public DataSet GetEDIDReport_Layer2(DataTable devicetypes, DataTable locationTypes, String brands, String models)
        {
            DataSet dsReport = null;
            DataTable dtEDID = null;
            DataTable dtLocationWithCode = null;
            this.ErrorMessage = String.Empty;
            try
            {
                //Get All Locations with Code
                dtLocationWithCode = this.m_ADOService.GetLocationWithCodeFromServer();
                if (dtLocationWithCode != null)
                    dtLocationWithCode.TableName = "Locations";

                //Get All IP Data
                dtEDID = this.m_ADOService.GetEDIDData_Layer2(); //this.m_ADOService.GetIPFromServer(String.Empty, String.Empty);
                if (dtEDID != null)
                    dtEDID.TableName = "EDID";

                dsReport = new DataSet();
                if (dtEDID != null)
                    dsReport.Tables.Add(dtEDID);
                dsReport.Tables.Add(dtLocationWithCode);

                this.ErrorMessage = this.m_ADOService.ErrorMessage;
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
            return dsReport;
        }
        public DataSet GetInfoFrameReport_Layer2(DataTable devicetypes, DataTable locations, String brands, String models)
        {
            DataSet dsReport = null;
            DataTable dtInfoFrame = null;
            DataTable dtLocationWithCode = null;
            this.ErrorMessage = String.Empty;
            try
            {
                //Get All Locations with Code
                dtLocationWithCode = this.m_ADOService.GetLocationWithCodeFromServer();
                if (dtLocationWithCode != null)
                    dtLocationWithCode.TableName = "Locations";

                //Get All InfoFrame Data
                dtInfoFrame = this.m_ADOService.GetInfoFrameData_Layer2(String.Empty, String.Empty, String.Empty);
                if (dtInfoFrame != null)
                    dtInfoFrame.TableName = "InfoFrame";
                dsReport = new DataSet();
                if (dtInfoFrame != null)
                    dsReport.Tables.Add(dtInfoFrame);
                dsReport.Tables.Add(dtLocationWithCode);

                this.ErrorMessage = this.m_ADOService.ErrorMessage;
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
            return dsReport;
        }
        public DataSet GetInfoFrameReport_Layer1(DataTable devicetypes, DataTable locations, String brands, String models)
        {
            DataSet dsReport = null;
            DataTable dtInfoFrame = null;
            DataTable dtLocationWithCode = null;
            this.ErrorMessage = String.Empty;
            try
            {
                //Get All Locations with Code
                dtLocationWithCode = this.m_ADOService.GetLocationWithCodeFromServer();
                if (dtLocationWithCode != null)
                    dtLocationWithCode.TableName = "Locations";

                //Get All InfoFrame Data
                dtInfoFrame = this.m_ADOService.GetInfoFrameData_Layer1(String.Empty, String.Empty, String.Empty);
                if (dtInfoFrame != null)
                    dtInfoFrame.TableName = "InfoFrame";
                dsReport = new DataSet();
                if (dtInfoFrame != null)
                    dsReport.Tables.Add(dtInfoFrame);
                dsReport.Tables.Add(dtLocationWithCode);

                this.ErrorMessage = this.m_ADOService.ErrorMessage;
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
            return dsReport;
        }
        #endregion

        #region CEC Captured Device Details
        public void GetCapturedDeviceDetails(DeviceTypes devicetypes, Locations locations, List<String> brands, List<String> models)
        {
            this.ErrorMessage = String.Empty;
            DataRow dr = null;
            //Device Type Table
            DataTable dtDeviceType = new DataTable();
            dtDeviceType.Columns.Add("MainDevice");
            dtDeviceType.Columns.Add("SubDevice");
            dtDeviceType.AcceptChanges();

            if (devicetypes != null)
            {                
                foreach (DeviceType devicetype in devicetypes)
                {
                    dr = dtDeviceType.NewRow();
                    dr["MainDevice"] = devicetype.MainDevice;
                    foreach (String subdevice in devicetype.SubDevice)
                    {
                        if(dr == null)
                            dr["MainDevice"] = devicetype.MainDevice;
                        dr["SubDevice"] = subdevice;
                        dtDeviceType.Rows.Add(dr);
                        dr = null;
                    }
                    if(dr != null)
                        dtDeviceType.Rows.Add(dr);
                    dr = null;
                }
            }
            dtDeviceType.AcceptChanges();
            dr = null;

            //Location Type Table
            DataTable dtLocationType = new DataTable();
            dtLocationType.Columns.Add("Region");
            dtLocationType.Columns.Add("Country");
            dtLocationType.AcceptChanges();
            if (locations != null)
            {
                foreach (Location region in locations)
                {
                    dr = dtLocationType.NewRow();
                    dr["Region"] = region.Region;
                    foreach (String country in region.Country)
                    {
                        if(dr == null)
                            dr["Region"] = region.Region;
                        dr["Country"] = country;
                        dtLocationType.Rows.Add(dr);
                        dr = null;
                    }
                    if(dr != null)
                        dtLocationType.Rows.Add(dr);
                    dr = null;
                }
                dtLocationType.AcceptChanges();
            }
            //Brands
            String strBrand = String.Empty;
            if (brands != null)
            {
                foreach (String brand in brands)
                {
                    strBrand += brand + ",";
                }
                if (strBrand.Length > 0)
                    strBrand = strBrand.Remove(strBrand.Length - 1, 1);
            }
            //Models
            String strModel = String.Empty;
            if (models != null)
            {
                foreach (String model in models)
                {
                    strModel += model + ",";
                }
                if (strModel.Length > 0)
                    strModel = strModel.Remove(strModel.Length - 1, 1);
            }
            try
            {
                DataTable dtResult = this.m_ADOService.GetCECCapturedDeviceDetails(dtDeviceType, dtLocationType, strBrand, strModel);
                this.ErrorMessage = this.m_ADOService.ErrorMessage;
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
        }
        #endregion

        #region CEC Captured Device Key Details
        public void GetCapturedDeviceKeyDetails(String maindevicetype, String subdevicetype, String brand, String model)
        {
            this.ErrorMessage = String.Empty;
            try
            {
                DataTable dtResult = this.m_ADOService.GetCECCapturedDeviceKeyDetails(maindevicetype, subdevicetype, brand, model);
                this.ErrorMessage = this.m_ADOService.ErrorMessage;
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
        }
        #endregion

        #region CEC Captured Device Function Details
        public void GetCapturedDeviceFunctionDetails(String maindevicetype, String subdevicetype, String brand, String model)
        {
            this.ErrorMessage = String.Empty;
            try
            {
                DataTable dtResult = this.m_ADOService.GetCECCapturedDevicefunctionDetails(maindevicetype, subdevicetype, brand, model);
                this.ErrorMessage = this.m_ADOService.ErrorMessage;
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
        }
        #endregion

        #region CEC Device Details
        public DeviceTypes DeviceType()
        {
            Quickset.Model.CEC.DeviceTypes DeviceTypeCollection = new Quickset.Model.CEC.DeviceTypes();
            this.m_ErrorMessage = string.Empty;

            try
            {
                //DataTable DeviceTable = this.m_ADOService.GetAvailableDeviceTypesForCEC();
                DataTable DeviceTable = this.m_ADOService.GetDeviceTypesFromServer();
                var groupedDevice = from row in DeviceTable.AsEnumerable()
                                    group row by row.Field<string>("MainDevice") into RegionGroup
                                    select new
                                    {
                                        MainDevice = RegionGroup.Key,
                                        SubDevice = RegionGroup.Select(row => row.Field<string>("SubDevice")).Distinct().ToList<string>(),
                                    };

                foreach (var data in groupedDevice)
                {
                    DeviceTypeCollection.Add(new DeviceType() { MainDevice = data.MainDevice, SubDevice = data.SubDevice });
                }
                this.ErrorMessage = m_ADOService.ErrorMessage;

            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
            return DeviceTypeCollection;
        }
        #endregion

        #region CEC Location Details
        public Locations Locations()
        {
            this.m_ErrorMessage = string.Empty;
            Quickset.Model.CEC.Locations Locationcoll = new Quickset.Model.CEC.Locations();
            try
            {
                //DataTable LocationTable = this.m_ADOService.GetAvailableLocationsForCEC();
                DataTable LocationTable = this.m_ADOService.GetLocationsFromServer();

                var groupedLocation = from row in LocationTable.AsEnumerable()
                                      group row by row.Field<string>("Region") into RegionGroup
                                      select new
                                      {
                                          Region = RegionGroup.Key,
                                          Country = RegionGroup.Select(row => row.Field<string>("Country")).ToList<string>(),
                                      };

                foreach (var data in groupedLocation)
                {
                    Locationcoll.Add(new Location() { Region = data.Region, Country = data.Country });
                }
                this.ErrorMessage = m_ADOService.ErrorMessage;

            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
            return Locationcoll;

        }
        #endregion

        #region Infoframe Data Import Layer - 1
        private DataTable FillInfoframedData(DataTable datatableFrom, DataTable datatableTo)
        {
            DataRow dr = null;
            for (Int32 intRows = 0; intRows < datatableFrom.Rows.Count; intRows++)
            {
                dr = datatableTo.NewRow();
                for (Int32 intColumn = 0; intColumn < datatableFrom.Columns.Count; intColumn++)
                {
                    if (dr.Table.Columns.Contains(datatableFrom.Columns[intColumn].ColumnName))
                        dr[datatableFrom.Columns[intColumn].ColumnName] = datatableFrom.Rows[intRows][intColumn];                       
                }                
                datatableTo.Rows.Add(dr);
                dr = null;
            }
            return datatableTo;
        }
        public DataSet InfoframeDataImport_Layer1(DataTable captureDevices, ref String strStatusMessage)
        {
            DataSet dsResult = null;
            this.ErrorMessage = String.Empty;
            try
            {
                DataTable datatableCaptureDevices = null;
                //Get SchemaTable from Server
                strStatusMessage = "Reading Schema Table for " + captureDevices.TableName;
                datatableCaptureDevices = this.m_ADOService.GetCapturedTableType("InfoframeInputData_Layer1");

                captureDevices.Columns["Device Type"].ColumnName = "DeviceType";
                captureDevices.Columns["Info Frame Raw Data"].ColumnName = "Infoframe Raw Data";               

                //Fill DataTable
                strStatusMessage = "Filling " + captureDevices.TableName + " from Infoframe Template";
                datatableCaptureDevices = FillInfoframedData(captureDevices, datatableCaptureDevices);
                 
                strStatusMessage = "Importing Infoframe Captured Details...";
                dsResult = this.m_ADOService.ImportInfoframeData_Layer1(datatableCaptureDevices);
                this.ErrorMessage = this.m_ADOService.ErrorMessage;

                if (dsResult != null)
                {
                    dsResult.Tables[0].TableName = "InfoFrame Capture Details";
                   // dsResult.Tables[1].TableName = "CEC Keys";
                   // dsResult.Tables[2].TableName = "Command List";
                }
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
            return dsResult;
        }
        #endregion

        #region Brand
        public DataTable Brands()
        {
            this.ErrorMessage = String.Empty;
            DataTable dtResult = new DataTable();
            try
            {
                dtResult = this.m_ADOService.GetBrandsFromServer();
                this.ErrorMessage = this.m_ADOService.ErrorMessage;
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
            return dtResult;
        }
        #endregion

        #region Model
        public DataTable Model()
        {
            this.ErrorMessage = String.Empty;
            DataTable dtResult = new DataTable();
            try
            {
                //dtResult = this.m_ADOService.GetModelsFromServer();
                this.ErrorMessage = this.m_ADOService.ErrorMessage;
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
            return dtResult;
        }
        #endregion
    }
}